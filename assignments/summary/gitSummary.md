# Git
![Git](https://avatars3.githubusercontent.com/u/18133?s=200&v=4)  

Git is a free and widely used open source distributed version control system. It is also becoming the standard for version control.  
With Git, you can easily:
- Have a place where you can store your code.
- Keep track of your projects history,so that you can revert back to your older version if your current version fails or to compare code with your older verion.
- Collaborate with others.
- can extremely work fast.
### Keywords
 ###### Repository:
 It is a central place where your code files and folders are stored in an organised way.It is shortly called as repo.  
 ###### Commit:
 Commiting means saving your work.  
 ###### Push: 
 Pushing means syncing your commits to gitlab.
 ###### Branch:
 Branch is simply a movable pointer to the commits. _Master_ is the default branch name in git.
 ###### Merge:
 Merging is integrating two branches together.
 ###### Clone and fork:
 Cloning of a repo means making a copy of your online repo and forking is making a existing repo on your local machine.
## Git Workflow
![Basic Git Workflow](https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)
